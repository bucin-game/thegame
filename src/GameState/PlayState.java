package GameState;

import java.awt.AlphaComposite;
import java.awt.Color;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import Assets.Assets;
import Entities.EntityManager;
import Entities.HUD;
import Entities.Player;
import Handlers.Handlers;
import Handlers.Keys;
import Level.Level;
import Main.GamePanel;
import Utils.ImageLoader;

public class PlayState extends GameState {

	// testing opacity
	private float alpha;

	private Level level1;
	private HUD hud;
	private BufferedImage test;
	private EntityManager entityManager;
	private int currentLevel;
	private String level;
	private int lastLevel;
	private boolean finished;

	private long timer;
	private long lastTime;

	private float rate;

	public PlayState(Handlers handler) {
		super(handler);
		this.timer = 0;
		this.lastTime = 0;

		this.lastLevel = 3;
		this.currentLevel = 1;
		this.level = "Assets/Level/level";
		this.rate = 0.02f;
		hud = new HUD(handler);
		handler.getDisplay().time();
		init();
	}

	@Override
	public void init() {

		this.timer = 0;
		this.lastTime = 0;

		finished = false;
		System.out.println(timer);
		handler.setPlayState(this);
		test = ImageLoader.loadImage("Background/entah1.jpg");
		System.out.println("LOADING " + level + currentLevel + ".txt");
		level1 = new Level(handler, level + currentLevel + ".txt");
		handler.setLevel(level1);
		entityManager = new EntityManager(handler,
				(new Player(handler, level1.getSpawnXP1(), level1.getSpawnYP1(), Player.size, Player.size)),
				(new Player(handler, level1.getSpawnXP2(), level1.getSpawnYP2(), Player.size, Player.size)));

		entityManager.setFinishTile(
				level1.CreateTiles(level1.getFinishTileType(), level1.getFinishTileX(), level1.getFinishTileY()));
		entityManager.getPlayer2().setTexture(Assets.player2);
		level1.populateWorld();

		this.alpha = 0;

		
		


	}

	@Override
	public void update() {
		// level1.update();

		handleInput();
		entityManager.update();
		isFinished();
	}

	@Override
	public void draw(Graphics2D g) {

		g.setColor(Color.BLACK);
		g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);

		// set the opacity
		g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// do the drawing here

		g.drawImage(test, 0, 0, GamePanel.WIDTH, GamePanel.HEIGHT, null);

		hud.draw(g);

		entityManager.draw(g);
		// increase the opacity and repaint
		alpha += rate;
		if (alpha >= 1.0f) {
			alpha = 1.0f;
		}
	}

//	@Override
//	public void draw(Graphics2D g) {
//		//g.setColor(Color.BLACK);
//		//g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
//		g.setColor(Color.WHITE);
//		
//		g.drawImage(test, 0, 0, GamePanel.WIDTH, GamePanel.HEIGHT,null);
//		
////		level1.draw(g);
//		
//		entityManager.draw(g);
//		
//	}

	private void isFinished() {

		if (entityManager.isPlayer1OnFinishTile() && entityManager.isPlayer2OnFinishTile()) {
			finished = true;
//			handler.getDisplay().setSecond(0);
//			handler.getDisplay().setMinute(0);
//			handler.getDisplay().setPause(true);
//			handler.getDisplay().getTimer().cancel(true);
			if (currentLevel < lastLevel) {
				if (timer > 2000000) {
					currentLevel++;
					handler.getEntityManager().getEntities().clear();
					this.init();
				}
			}

			else {
				handler.getGsm().setState(GameStateManager.MENUSTATE);
			}
			timer += System.nanoTime() - lastTime;
			lastTime = System.nanoTime();
			System.out.println(timer);
		} else {
			timer = 0;
			lastTime = 0;
			finished = false;
		}
	}

	@Override
	public void handleInput() {
		if (!finished) {
			entityManager.getPlayer().setLeft(Keys.keyState[Keys.LEFT]);
			entityManager.getPlayer().setRight(Keys.keyState[Keys.RIGHT]);
			entityManager.getPlayer().setDown(Keys.keyState[Keys.DOWN]);
			entityManager.getPlayer().setJumping(Keys.keyState[Keys.UP]);

			entityManager.getPlayer2().setLeft(Keys.keyState[Keys.LEFT_PLAYER2]);
			entityManager.getPlayer2().setRight(Keys.keyState[Keys.RIGHT_PLAYER2]);
			entityManager.getPlayer2().setDown(Keys.keyState[Keys.DOWN_PLAYER2]);
			entityManager.getPlayer2().setJumping(Keys.keyState[Keys.UP_PLAYER2]);
		}
	}

	// GETTER SETTER
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public Level getLevel1() {
		return level1;
	}

}
