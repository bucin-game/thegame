package Main;

import java.awt.Canvas;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

import Handlers.Handlers;

public class Display {

	private JScrollPane tabel;
	private DefaultTableModel defTableModel;
	private JTable kampret;
	private JFrame frame;
	private Canvas canvas;
	private Handlers handler;
	SwingWorker<Void, Integer> timer;
	private String title;
	private int width, height;
	private String player1;
	private String player2;
	private int second;
	private int minute;
	private boolean pause;

	public Display(String title, int width, int height) {
		this.title = title;
		this.width = width;
		this.height = height;

		createDisplay();
	}

	private void createDisplay() {
		frame = new JFrame(title);
		frame.setSize(width, height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setFocusable(true);
		frame.requestFocus();

		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(width, height));
		canvas.setMaximumSize(new Dimension(width, height));
		canvas.setMinimumSize(new Dimension(width, height));
		canvas.setFocusable(false);

		frame.add(canvas);
		frame.pack();
	}

	public Canvas getCanvas() {
		return canvas;
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setHandler(Handlers handler) {
		this.handler = handler;
	}

	public void dialogPane(String hasil) {
		player1 = JOptionPane.showInputDialog("MASUKAN NAMA PLAYER 1");
		player2 = JOptionPane.showInputDialog("MASUKAN NAMA PLAYER 2");
		handler.getDatabase().addData(player1, player2, hasil);
	}

	public void highscore() {
		JFrame frame = new JFrame("Highscores");
		String[] columnNames = { "Player 1", "Player 2", "Waktu" };
		JPanel panel = new JPanel();
		defTableModel = new DefaultTableModel();
		defTableModel.setColumnIdentifiers(columnNames);

		kampret = new JTable();
		kampret.setModel(defTableModel);

		tabel = new JScrollPane();
		tabel.setViewportView(kampret);

		defTableModel.addRow(new Object[] { "kak", "sdas", "sadas" });

		panel.add(kampret);
		panel.setMinimumSize(new Dimension(200,200));
		frame.setPreferredSize(new Dimension(200, 200));
		
		JOptionPane.showMessageDialog(frame, panel);

	}

	public void time() {

		timer = new SwingWorker<Void, Integer>() {
			int seconds = 0;
			int minutes = 0;

//			int now = 0;
			@Override
			protected Void doInBackground() throws Exception {
//				int now = 0;
//				seconds = 0;
//				minutes = 0;
				// seconds = 0;
//				long lastTime;
//				long currentTime;
				long targetTime;

				targetTime = 999;

				while (!pause) {

					Thread.sleep(targetTime);
					seconds++;
					publish(seconds);
				}
				seconds = 0;
				minutes = 0;

				return null;
			}

			@Override
			protected void process(List<Integer> chunks) {
				if (seconds >= 60) {
					seconds = 0;
					minutes++;
				}

				setSecond(seconds);
				setMinute(minutes);
			}

			@Override
			protected void done() {
				// TODO Auto-generated method stub
				System.out.println("timer stopped");
			}

		};

		timer.execute();

	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public boolean isPause() {
		return pause;
	}

	public void setPause(boolean pause) {
		this.pause = pause;
	}

	public SwingWorker<Void, Integer> getTimer() {
		return timer;
	}

	public void setTimer(SwingWorker<Void, Integer> timer) {
		this.timer = timer;
	}

}
