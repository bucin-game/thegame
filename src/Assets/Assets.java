package Assets;

import java.awt.image.BufferedImage;

import Utils.ImageLoader;

public class Assets {
	
	public static final int HEIGHT = 50, WIDTH = 200;
	
	public static BufferedImage player, player2, woodBlock, blueBlock, pinkBlock , whiteBlock, longWhiteBlock, longBlueBlock, longPinkBlock, longWoodBlock;
	public static BufferedImage heart;
	public static void init() {
		SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("Object/picture3.png"));
		heart = ImageLoader.loadImage("HUD/heartsmall.png");
		woodBlock = sheet.crop(0, 0, WIDTH, HEIGHT);
		blueBlock = sheet.crop(WIDTH, 0, WIDTH, HEIGHT);
		pinkBlock = sheet.crop(WIDTH*2, 0, WIDTH, HEIGHT);
		whiteBlock = sheet.crop(WIDTH*3, 0, WIDTH, HEIGHT);
		longWhiteBlock = sheet.crop(0, HEIGHT*3, WIDTH*2, HEIGHT);
		longBlueBlock = sheet.crop(WIDTH*2, HEIGHT*3, WIDTH*2, HEIGHT);
		longPinkBlock = sheet.crop(0, HEIGHT*4, WIDTH*2, HEIGHT);
		longWoodBlock = sheet.crop(WIDTH*2, HEIGHT*4, WIDTH*2, HEIGHT);
		player = ImageLoader.loadImage("Object/blue.png");
		player2 = ImageLoader.loadImage("Object/pink.png");
		
	}
}
